#ifndef TASK_H_INCLUDED
#define TASK_H_INCLUDED
#include <string>

using namespace std;

class Task
{
public:
    unsigned int id;
    string description;

    Task(unsigned int new_id,string new_description)
    {
        id = new_id;
        description= new_description;
    }

};

#endif // TASK_H_INCLUDED
