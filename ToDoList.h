#ifndef TODOLIST_H_INCLUDED
#define TODOLIST_H_INCLUDED
#include <vector>
#include "Task.h"

class ToDoList
{
   public:
       vector<Task> tasks;
       ToDoList(){
            tasks.clear();
            }
       bool add_new_task(string description)
       {
           if (description.empty())return false;
            unsigned int new_id = static_cast<int>(tasks.size())+1;
            Task new_task(new_id,description);
            tasks.push_back(new_task);
            return true;
       };

};

#endif // TODOLIST_H_INCLUDED
